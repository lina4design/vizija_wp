#vizija

Redesigning [this](http://www.kurybinesdirbtuves.lt) website.

Plan
-------
###1.Prepare WordPress project
1. Download WordPress files
2. Create custom theme `style.css`
###2.Project
1. Homepage
  1. Parts
  2. Header
  3. Navigation
  4. Section - Hero
  5. Section - Products
  6. Section - Hero
  7. Section - Products 2
  8. Section - Contact Us
  9. Footer nav
  10. Convert into WordPress
  11. Make items editable from wordpress